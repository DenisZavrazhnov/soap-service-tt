package ru.dzavrazhnov.soapdemo.endpoint;


import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.dzavrazhnov.spring_boot_soap_example.GetUserRequest;
import ru.dzavrazhnov.spring_boot_soap_example.GetUserResponse;

@Endpoint
public class UserEndpoint {

    @PayloadRoot(namespace = "http://dzavrazhnov.ru/spring-boot-soap-example",localPart = "getUserRequest")
    @ResponsePayload
    public GetUserResponse getUserRequest(@RequestPayload GetUserRequest request){
        GetUserResponse response = new GetUserResponse();
        response.setResponse("Чистый доход по клиенту: " + request.getSecondName() + " " +
                             request.getFirstName() + " " +
                             request.getLastName() + " составляет " + (request.getIncome() - request.getExpensive()) + " рублей!");
        return response;
    }
}
